﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using LineBotWebHook.Models;
using Newtonsoft.Json;
using System.IO;
using LineBotLibrary;
using LineBotLibrary.Medels;

namespace LineBotWebHook.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class LineBotController : ControllerBase
    {
        LineBot himproBot = new LineBot("IatYsT/q5IQdB04t89/1O/w1cDnyilFU="); //Channel access token
        // GET: api/LineBot
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/LineBot/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/LineBot
        [HttpPost]
        public void Post(RequestMessages request)
        {
            //test
            string result = JsonConvert.SerializeObject(request);

            LineBotLibrary.Medels.MessageData mesData = new LineBotLibrary.Medels.MessageData();

            mesData.to = "Ubf80..."; //ผู้รับ
            string jsonMessage = request.events[0].message.ToString();

            LineBotLibrary.Medels.Message messa = new LineBotLibrary.Medels.Message();

            messa.text = ""; // ข้อความที่จะส่งถึงผู้รับ
            mesData.messages = new List<object>();
            mesData.messages.Add(messa);
            himproBot.PushMessage(mesData);
            string json = JsonConvert.SerializeObject(mesData);
            System.IO.File.WriteAllText("request.json", json);

        }

        // PUT: api/LineBot/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
