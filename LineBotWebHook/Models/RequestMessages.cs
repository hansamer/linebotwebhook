﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LineBotWebHook.Models
{
    public class RequestMessages
    {
        public string destination { get; set; }
        public List<Event> events { get; set; }
    }

    public class Source
    {
        public string type { get; set; }
        public string userId { get; set; }
    }

    public class Message
    {
        public string id { get; set; }
        public string type { get; set; }
        public string text { get; set; }

        //image & video
        public int duration { get; set; }
        public ContentProvider contentProvider { get; set; }

        //file
        public string fileName { get; set; }
        public int fileSize { get; set; }

        //localtion
        public string title { get; set; }
        public string address { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }

        //sticker
        public string packageId { get; set; }
        public string stickerId { get; set; }
    }

    public class Event
    {
        public string replyToken { get; set; }
        public string type { get; set; }
        public object timestamp { get; set; }
        public Source source { get; set; }
        public Message message { get; set; }
    }

    public class ContentProvider
    {
        //image & video
        public string type { get; set; }
        public string originalContentUrl { get; set; }
        public string previewImageUrl { get; set; }
    }
}
