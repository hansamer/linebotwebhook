﻿using System;
using System.Collections.Generic;
using LineBotLibrary.Medels;
using Newtonsoft.Json;
using RestSharp;
using System.Threading.Tasks;

namespace LineBotLibrary
{
    public class Class1
    {
    }

    public class LineBot
    {
        string Authorization { get; set; }

        public LineBot(string ChannelAccessToken)
        {
            Authorization = ChannelAccessToken;
        }

        public void PushMessage(Object messageData)
        {
            Task.Run(() =>
            {
                string json = JsonConvert.SerializeObject(messageData);
                var client = new RestClient("https://api.line.me/v2/bot/message/push");
                var request = new RestRequest(Method.POST);
                request.AddHeader("Postman-Token", "000ecc13-b789-4e9d-8cd7-b17524636e2d");
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Authorization", "Bearer {" + Authorization + "}");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("undefined", json, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
            });
        }
    }
    
}
