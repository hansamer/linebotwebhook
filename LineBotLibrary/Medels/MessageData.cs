﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LineBotLibrary.Medels
{
    public class MessageData
    {
        public string to { get; set; }
        public List<object> messages { get; set; }
        
    }

    public class Message
    {
        public readonly string type = "text";
        public string text { get; set; }
    }

    public class sticker
    {
        public readonly string type = "sticker";
        public string packageId { get; set; }
        public string stickerId { get; set; }
    }

    public class image
    {
        public readonly string type = "image";
        public string originalContentUrl { get; set; }
        public string previewImageUrl { get; set; }
    }

    public class video
    {
        public readonly string type = "video";
        public string originalContentUrl { get; set; }
        public string previewImageUrl { get; set; }
    }

    public class audio
    {
        public readonly string type = "audio";
        public string originalContentUrl { get; set; }
        public int duration { get; set; }
    }

    public class location
    {
        public readonly string type = "location";
        public string title { get; set; }
        public string address { get; set; }
        public double latitude { get; set; }
        public double longitude { get; set; }
    }
}
