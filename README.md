**Line bot WebHook **

พัฒนาบน ASP.Net Core MVC g
ภาษา C#

หลักการทำงานคือการสร้าง webservice เพื่อรับค่าจาก server ของไลน์ เมื่อมีผู้ใช้ส่งข้อความเข้ามาหา bot
code จะอยู่ในไฟล์ api/LineBotController.cs

code json ด้านล้างนี้คือตัวอย่างข้อความที่ไลน์ส่งกับมาให้เรา [ดูรายละเอียดเพิ่มเติม](https://developers.line.biz/en/reference/messaging-api/)

`{
  "destination": "xxxxxxxxxx", 
  "events": [
    {
      "replyToken": "0f3779fba3b349968c5d07db31eab56f",
      "type": "message",
      "timestamp": 1462629479859,
      "source": {
        "type": "user",
        "userId": "U4af4980629..."
      },
      "message": {
        "id": "325708",
        "type": "text",
        "text": "Hello, world"
      }
    },
    {
      "replyToken": "8cf9239d56244f4197887e939187e19e",
      "type": "follow",
      "timestamp": 1462629479859,
      "source": {
        "type": "user",
        "userId": "U4af4980629..."
      }
    }
  ]
}`

` LineBot himproBot = new LineBot("IatYsT/q5IQdB04t89/1O/w1cDny..."); //Channel access token`
ส่วนนีเอา push message


`// POST: api/LineBot
        [HttpPost]
        public void Post(RequestMessages request)
        {
            //test
            string result = JsonConvert.SerializeObject(request);

            LineBotLibrary.Medels.MessageData mesData = new LineBotLibrary.Medels.MessageData();

            mesData.to = "Ubf80..."; //ผู้รับ
            string jsonMessage = request.events[0].message.ToString();

            LineBotLibrary.Medels.Message messa = new LineBotLibrary.Medels.Message();

            messa.text = ""; // ข้อความที่จะส่งถึงผู้รับ
            mesData.messages = new List<object>();
            mesData.messages.Add(messa);
            himproBot.PushMessage(mesData);
            string json = JsonConvert.SerializeObject(mesData);
            System.IO.File.WriteAllText("request.json", json);

        }`